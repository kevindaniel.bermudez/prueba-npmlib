module.exports = (dirname,methodName) => {
  try { 
    const path = `${dirname}/methods/${methodName}`;
    const method = require(path);
    return method()
  }
  catch (error) {
    const path = `../methods/${methodName}`;
    const method = require(path);
    return method()
  }
}